<?php
/*
	此为管理路口文件
*/
error_reporting(0);

header('Content-Type: text/html; charset=GBK');
date_default_timezone_set('Asia/Shanghai');
define('dingwork_path',dirname(__FILE__));
define('app_path',dingwork_path.'/app/index/');

set_include_path(
	get_include_path() .
	PATH_SEPARATOR.dingwork_path
);

require_once 'dingwork/FrameWork.php';
App::run();