<?php
class TypeModelDb extends ModelDb{
	protected $_table = 'type';
	public function tlist(){
		return $this->where('pid = 0')->select();
	}
	public function GetTypeArray(){
		foreach($this->where('pid > 0')->select() as $v){
			$types[$v['pid']][$v['id']] = $v;
		}
		return $types;
	}
}