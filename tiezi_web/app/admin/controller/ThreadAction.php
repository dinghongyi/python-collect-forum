<?php
import('Page','','');
import('Html','','');
import('CommonAction','','');
class Thread_Action extends Common_Action{
	public function init(){
		parent::init();
		$this->redis = new Redis();
		$this->redis->connect('127.0.0.1', 6379);
		$this->redis->select($this->_config['redisdb']);
		$this->thread = new ModelDB('thread');
	}
	public function do1(){
		$_SESSION['re'] = $_SERVER['HTTP_REFERER'];
		switch($_POST['do']){
			case '审核':$this->doshow();break;
			case '去属性':$this->dodel();break;
			case '加属性':$this->doadd();break;
			case '移动':$this->domove();break;
		}
		$this->_router->redirct($_SESSION['re'],true);
	}
	private function doadd(){
		if($_POST['id'] && $_POST['flag']){
			foreach($_POST['id'] as $v){
				$result = $this->thread->where(sprintf("id = %d",$v))->find();
				$array = array('flag'=> (!empty($result['flag']) ? $result['flag'].',' : '').$_POST['flag']);
				$this->thread->where(sprintf("id = %d",$v))->update($array);
			}
		}
	}
	private function dodel(){
		if($_POST['id'] && $_POST['flag']){
			foreach($_POST['id'] as $v){
				$result = $this->thread->where(sprintf("id = %d",$v))->find();
				$flag = explode(',',$result['flag']);
				$t = array_search($_POST['flag'],$flag);
				if($t !== false){
					unset($flag[$t]);
					$array = array('flag'=>implode(',',$flag));
					$this->thread->where(sprintf("id = %d",$v))->update($array);
				}
			}
		}
	}
	private function doshow(){
		if($_POST['id']){
			$array = array('show'=>1);
			foreach($_POST['id'] as $v){
				$this->thread->where(sprintf("id = %d",$v))->update($array);
			}
		}
	}
	private function domove(){
		if($_POST['id']){
			$array = array('tid2' => $_POST['tid2']);
			foreach($_POST['id'] as $v){
				$this->thread->where(sprintf("id = %d",$v))->update($array);
			}
		}
	}
	public function index(){
		$this->gettype();
		$this->uri();
		try{
			$page = $_GET['page'];
			$limit = $this->_config['list_limit'];

			$where = '1 =1 ';
			$keyword = $_GET['keyword'];
			if(!empty($keyword)){
				$where .= "and (CONCAT( subject, body ) REGEXP '$keyword')";
			}
			$flag= $_GET['flag'];
			if(!empty($flag)){
				$where.=" and flag ='$flag'";
			}
			$orderby = $_GET['orderby'];
			if(!empty($orderby)){
				$orderby ="$orderby desc";
			}else{
				$orderby = 'id desc';
			}
			$tid2 = $_GET['tid2'];
			if(!empty($tid2)){
				$where .=" and tid2 = $tid2";
			}
			$show = $_GET['show'];
			if(!empty($show)){
				$where .= ' and `show` = '.($show ==2 ? 0:1);
			}
			$this->thread->where($where);
			$rid = sprintf('admin_list_c_%s',md5($keyword.$flag.$tid2.$show));
			if(!$this->redis->exists($rid)){
				$c = $this->thread->fields('count(*) as c')->find();
				$this->redis->set($rid,$c['c'],3600);
				$c = $c['c'];
			}else{
				$c = $this->redis->get($rid);
			}
			//$this->redis->delete($rid);
			if(empty($page) or $page == 1){
				$offset = 0;
				$page=1;
            }else
                $offset = ($page-1) * $limit;
			$pagecount = ceil($c/$limit);


			if ($pagecount ==0) $pagecount=1;
			$this->thread->where($where);
			$result = $this->thread->limit("{$offset},{$limit}")->orderby($orderby)->select();
			$this->_view->assign('page',array('page'=>$page,'pagecount'=>$pagecount));
			$this->_view->assign('list',$result);

			$this->_view->display('admin/thread.htm');
		}catch(Exception $e){
			print_r($e);
		}
	}
	public function typeupdate(){
		$this->type = new ModelDb('type');
		$this->redis->flushdb();
		$this->gettype();
		echo '更新成功';
	}
	private function gettype(){
		$type = new ModelDB('type');
		$rid= 'gettype';
		if($this->redis->exists($rid)){
			$typelist = unserialize($this->redis->get($rid));
		}else{
			foreach($type->fields('id,pid,name,pinyin')->select() as $v){
				$typelist[$v['id']] = $v;
				$tid2[$v['id']] = $v['pinyin'];
			}
			$this->redis->set($rid,serialize($typelist));
			$this->redis->set(sprintf('tid2_%s',$rid),serialize($tid2));
		}
		//#主分类id
		$rid = 'gettype2';
		if($this->redis->exists($rid)){
			$tlist = unserialize($this->redis->get($rid));
		}else{
			foreach($type->fields('id,name,pinyin')->where('pid = 0')->select() as $v){
				$tlist[$v['id']] = $v['name'];
				$tid[$v['id']] = $v['pinyin'];
			}
			$this->redis->set($rid,serialize($tlist));
			$this->redis->set(sprintf('tid_%s',$rid),serialize($tid));
		}
		$this->_view->assign('tid2',$typelist);
		$this->_view->assign('tid',$tlist);

	}
	private function uri(){
		$keyword = rawurlencode($_GET['keyword']);
		$flag= $_GET['flag'];
		$orderby = $_GET['orderby'];
		$tid2 = $_GET['tid2'];
		if(!empty($_GET['show']))
			$show = $_GET['show'] == 2?0:1;
		$uri = "?keyword=$keyword&flag=$flag&orderby=$orderby&tid2=$tid2&show=$show";
		$this->_view->assign('uri',$uri);
	}

}

?>