<?php
import('TypeModelDb');
class Type_Action extends Action{
	public function init(){
		$this->type = new TypeModelDb();
	}
	public function index(){
		$this->_view->assign('tlist',$this->type->tlist());
		$this->_view->assign('plist',$this->type->GetTypeArray());
		$this->_view->display('admin/type.htm');
	}
	public function do1(){
		$_SESSION['re'] = $_SERVER['HTTP_REFERER'];
		if($_POST['add'] == 1){
			$arr = array('name'=>$_POST['typename'],'pid'=>intval($_POST['ptype']),'pinyin'=>$_POST['pinyin']);
				if(!$this->type->add($arr)){
					echo '添加失败';exit();
				}
		}
		$this->_router->redirct($_SESSION['re'],true);
	}
	public function do2(){
		$_SESSION['re'] = $_SERVER['HTTP_REFERER'];
		if($_POST){
			$id = intval($_POST['id']);
			$arr = array('name'=>$_POST['typename'],'pinyin'=>$_POST['pinyin']);
			$this->type->where('id = '.$id)->update($arr);
		}
		$this->_router->redirct($_SESSION['re'],true);
	}

}