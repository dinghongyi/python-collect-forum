<?php
import('CommonAction','','');
class Keywords_Action extends Common_Action{
	public function init(){
		parent::init();
		$this->keyword = new ModelDB('keywords');
		$this->redis = new Redis();
		$this->redis->connect('127.0.0.1', 6379);
		$this->redis->select($this->_config['redisdb']);
		
	}
	
	public function index(){
		$this->_view->assign('keywordlist',$this->keyword->select());
		$this->_view->display('admin/keyword.htm');
		
	}
	public function add(){
		if($_POST){
			$method = $_POST;
			$arr = array('name'=>$method['keyword'],'url'=>$method['url']);
			$this->keyword->add($arr);
			$this->_router->redirct('/__admin__.php/keywords/',true);
		}
		$this->redis->delete('view_keyword');
	}
	public function delete(){
		$kid= $this->_request->getParam('kid');
		$this->keyword->where(sprintf('`kid` = %d',$kid))->delete();
		$this->redis->delete('view_keyword');
		$this->_router->redirct('/__admin__.php/keywords/',true);
	}
	public function edit(){
		if($_POST){
			$method = $_POST;
			$arr = array('name'=>$method['keyword'],'url'=>$method['url']);
			$kid = $method['kid'];
			$this->keyword->where(sprintf('`kid` = %d',$kid))->update($arr);
			$this->redis->delete('view_keyword');
			$this->_router->redirct('/__admin__.php/keywords/',true);
		}else{
			$kid= $this->_request->getParam('kid');
			$this->_view->assign('keyword',$this->keyword->where(sprintf('`kid` = %d',$kid))->find());
			$this->_view->display('admin/keyword_edit.htm');
		}
	}
	
}

?>