<?php
import('Html','','');
import('CommonAction','','');
class Var_Action extends Common_Action{
	public function init(){
		parent::init();
		$this->thread = new ModelDB('thread');
	}
	public function index(){
		$id = sprintf('%d',$this->_request->getParam('id'));
		if($_POST){
			$array['pubdate'] = time();
			$array['body'] = stripslashes($_POST['body']);
			$array['show'] = $_POST['show']?1:0;
			$array['flag'] = implode(',',$_POST['flag']);
			$array['u'] = intval($_POST['u']);
			$array['litpic'] = str_replace('http%3A','http:',trim($_POST['litpic']));
			$array['subject'] = stripslashes($_POST['subject']);
			$this->thread->where("id = $id")->update($array);
			$this->_router->redirct($_SESSION['re'],true);
		}else{
			$_SESSION['re'] = $_SERVER['HTTP_REFERER'];
		}
		$result = $this->thread->where("id = $id")->find();
		$result['flag'] = explode(',',$result['flag']);
		$this->_view->assign('one',$result);
		$this->_view->display('admin/id.htm');
	}
}

?>