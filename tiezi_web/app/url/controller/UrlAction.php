<?php
class Url_Action extends Action{

	public function index(){
		$arr = array('tt.mop.com'=>1,'dzh.mop.com'=>2,'www.douban.com'=>3,'tieba.baidu.com'=>4,'www.shszc.com'=>5,'bbs.tianya.cn'=>6);
		$url = base64_decode($_GET['url']);
		$urlinfo = parse_url($url);
		$url = $urlinfo['path'] .($urlinfo['query']?"?{$urlinfo['query']}":'');
		$this->_view->assign('classid',$arr[$urlinfo['host']]);
		$this->_view->assign('url',$url);
		$this->_view->display('url/index.htm');
	}
}