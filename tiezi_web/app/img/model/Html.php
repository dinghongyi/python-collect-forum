<?php

function dir_create($directoryname){
    $directoryname = str_replace("", "/", $directoryname);
    $dirnames = explode('/', $directoryname);
    $total = count($dirnames) ;
    $temp = '';
    for($i = 0; $i < $total; $i++){
        $temp .= $dirnames[$i] . '/';
        if (!is_dir($temp)){
            $oldmask = umask(0);
            if (!mkdir($temp, 755)) exit("不能建立目录 $temp");
            umask($oldmask);
            }
        }
    return true;
    }

function downloadfile($img,$path,$file,$referer){
	$fp = curl_init();
	curl_setopt($fp, CURLOPT_HTTPGET, true);
	curl_setopt($fp, CURLOPT_USERAGENT,"Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2)");
	curl_setopt($fp, CURLOPT_REFERER, "http://".$referer);
	curl_setopt($fp, CURLOPT_URL,$img);
	curl_setopt($fp, CURLOPT_RETURNTRANSFER,true);
	$content = curl_exec($fp);
	curl_close($fp);
	file_put_contents($path.'/'.$file,$content);
}