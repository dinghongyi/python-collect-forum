<?php
function page($page,$pagecount,$c=10,$pc=5,$nc=4){
	$ts = $page+$pc > $pagecount ?($pagecount+1-$c):($page<$pc?1:$page-$nc);
	return range(($ts > 1)?$ts:1,(($page+$pc)>=$pagecount) ?($pagecount):( ($pagecount+1)>=$c?$c: ($pagecount+1) ) + ($page - ( $page>$pc?$nc:($page-1) ) ) + ( ($page<$nc and $pagecount<10)?-1:0 ) -1);

}
function wheretime($col,$type){
	$etime = $starttime = $endtime = 0;
	$time = time();
    $etime = getdate($time);
	$arrtime = localtime($time,true);
	//print_r();
	switch($type){
		case 'curdate':
		     $starttime = mktime(0, 0, 0, $etime['mon'], $etime['mday'], $etime['year']);
			 $endtime = mktime(23, 59, 59, $etime['mon'], $etime['mday'], $etime['year']);
		break;
		case 'curweek':
		     $starttime = mktime(0, 0, 0, $etime['mon'], $etime['mday']-$arrtime['tm_wday'], $etime['year']);
			 $endtime = mktime(23, 59, 59, $etime['mon'], $etime['mday'], $etime['year']);
		break;
		case 'curmonth':
		     $starttime = mktime(0, 0, 0, $etime['mon'], 1, $etime['year']);
			 $endtime = mktime(23, 59, 59, $etime['mon'], $etime['mday'], $etime['year']);
		break;
	}
	return "(`$col` >= $starttime AND `$col` <= $endtime)";
}
// print_r(wheretime('pubdate','curmonth'));
// print_r(date('Y-m-d H:i:s',$starttime));
//print_r(page(1,10));
#
#def pagelist(page,pagecount,c=10,pc=5,nc=4):
##    ts = (pagecount+1-c if page+pc >pagecount else (1 if page < pc else page-nc))
#    return range(ts if ts > 1 else 1,\
#      pagecount+1 if page+pc >= pagecount else (c if (pagecount+1)>=c else (pagecount+1) ) +(page -(nc if page >pc else page-1)) + (-1 if page<nc and pagecount<10 else 0))
#
#
#