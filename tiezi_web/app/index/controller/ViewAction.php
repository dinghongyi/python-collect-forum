<?php
import('Page','','');
import('Html','','');
import('CommonAction','','');
class View_Action extends Common_Action{
	public function init(){
		parent::init();
		$this->viewkeyword();
	}
	public function index(){
		try{
			$this->top();
			$id = $this->_request->getParam('id');
			$page = $_GET['page'];
			$limit = $this->_config['vpage_list_limit'];

			$threads = new ModelDb('threads');
			$id = intval($id);
			$rid = sprintf('vid_%d',$id);
			
			if(!$this->redis->exists($rid)){
				$thread_result = $this->thread->where(sprintf('id = %d',$id))->find();
				$this->redis->set($rid,serialize($thread_result),87600);
			}else{
				$thread_result = unserialize($this->redis->get($rid));
			}
			
			if(!$thread_result){
				throw new Exception("没有这个id：{$id}的帖子");
			}


			$rid = sprintf('vc_%d',$id);
			if(!$this->redis->exists($rid)){
				$c = $threads->where(sprintf('aid = %d',$id))->fields('count(*) as c')->find();
				$this->redis->set($rid,$c['c'],87600);
				$c = $c['c'];
			}else{
				$c = $this->redis->get($rid);
			}

			if(empty($page) or $page == 1){
				$offset = 0;
				$page=1;
            }else
                $offset = ($page-1) * $limit;

			$pagecount = ceil($c/$limit);
			if ($pagecount ==0) $pagecount=1;
			$rid = sprintf('vp_%d_%d_%d',$id,$offset,$limit);
			if(!$this->redis->exists($rid)){
				$threads_result = $threads->where(sprintf('aid = %d',$id))->limit("{$offset},{$limit}")->orderby('id asc')->select();
				$keywordlist = unserialize($this->redis->get('view_keyword'));
				foreach($keywordlist as $v){
					$keyword[$v['name']]="<a href='{$v['url']}' title='{$v['name']}'>{$v['name']}</a>";
				}
				foreach($threads_result as $k=>$v){
					$threads_result[$k]['body'] = strtr($v['body'],$keyword);
				}
				$this->redis->set($rid,serialize($threads_result),86400);
			}else{
				$threads_result = unserialize($this->redis->get($rid));
			}

			$thread_result['url'] = base64_encode($thread_result['url']);
			$this->_view->assign('thread',$thread_result);
			$this->_view->assign('threads',$threads_result);
			$this->_view->assign('page',array('page'=>$page,'pagecount'=>$pagecount));
			$this->_view->display('tiezi/view.htm');
		}catch(Exception $e){
			DoException::exception($e);
		}

	}
	private function viewkeyword(){
		$rid = 'view_keyword';
		if(!$this->redis->exists($rid)){
			$this->keyword = new ModelDb("keywords");
			$result = $this->keyword->select();
			$this->redis->set($rid,serialize($result),86700);
		}
	}
	private function top(){
		$rid = 'top_view';
		if(!$this->redis->exists($rid)){
			$result = $this->thread->where('`show`=1')->orderby('id desc')->limit('0,10')->select();
			$this->redis->set($rid,serialize($result),1805);
		}else{
			$result = unserialize($this->redis->get($rid));
		}

		$this->_view->assign('top',$result);
	}

	public function sitemap(){
		$this->_view->display('tiezi/sitemap.htm');
	}
}