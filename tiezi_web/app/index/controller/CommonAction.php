<?php
class Common_Action extends Action{
	protected $tid = array();
	protected $tid2 = array();
	public function init(){
		$this->redis = new Redis();
		$this->redis->connect('127.0.0.1', 6379);
		$this->redis->select($this->_config['redisdb']);
		$this->thread = new ModelDb('thread');
		$this->gettype();
	}
	protected function gettype(){
		$type = new ModelDB('type');
		$rid= 'gettype';
		if($this->redis->exists($rid)){
			$typelist = unserialize($this->redis->get($rid));
			$tid2 = unserialize($this->redis->get(sprintf('tid2_%s',$rid)));
		}else{
			foreach($type->fields('id,pid,name,pinyin')->select() as $v){
				$typelist[$v['id']] = $v;
				$tid2[$v['id']] = $v['pinyin'];
			}
			$this->redis->set($rid,serialize($typelist));
			$this->redis->set(sprintf('tid2_%s',$rid),serialize($tid2));
		}
		//#������id
		$rid = 'gettype2';
		if($this->redis->exists($rid)){
			$tlist = unserialize($this->redis->get($rid));
			$tid = unserialize($this->redis->get(sprintf('tid_%s',$rid)));
		}else{
			foreach($type->fields('id,name,pinyin')->where('pid = 0')->select() as $v){
				$tlist[$v['id']] = $v['name'];
				$tid[$v['id']] = $v['pinyin'];
			}
			$this->redis->set($rid,serialize($tlist));
			$this->redis->set(sprintf('tid_%s',$rid),serialize($tid));
		}
		$this->tid = $tid;
		$this->tid2 = $tid2;
		$this->_view->assign('tid2',$typelist);
		$this->_view->assign('tid',$tlist);

	}
}