<?php
import('Page','','');
import('CommonAction','','');
class List_Action extends Common_Action{
	
	public function init(){
		parent::init();
	}
	public function index(){
		$this->lists('list');
	}
	public function name(){
		$tag = $_GET['tag'];
		$this->lists('name',$tag,'name');
	}
	public function tid(){
		$tag = $_GET['typeid'];
		if(preg_match('/^[0-9]{1,4}$/',$tag)){
			$tag = $tag;
		}elseif(preg_match('/^[A-Za-z0-9]{1,30}$/',$tag)){
			$tag = $this->pinyin($tag,1);
		}
		$this->lists('typeid',$tag,'tid');
	}
	public function tid2(){
		$tag = $_GET['tag'];
		$this->lists('type',$tag,'tid2');
	}
	protected function pinyin($py,$type){
		if($type ==1){
			return array_search($py,$this->tid);
		}elseif($type ==2){
			return array_search($py,$this->tid2);
		}
	}
	public function lists($tpl,$where='',$type=''){
		try{
			$page = $_GET['page'];
			$limit = $this->_config['list_limit'];


			if(empty($type))
				$rid = sprintf('lc_%s',$tpl);
			else
				$rid = sprintf('lc_%s_%s',$type,sha1($where));

			if(!empty($where)) $this->thread->where(sprintf("`show` = 1 and `%s` = '%s'",$type,$where));
			if(!$this->redis->exists($rid)){
				#$c = $this->thread->fields('count(*) as c')->find();
				#$this->redis->set($rid,$c['c'],3600);
				#$c = $c['c'];
			}else{
				#$c = $this->redis->get($rid);
			}
			$c = 2000;
			//$this->redis->delete($rid);
			if(empty($page) or $page == 1){
				$offset = 0;
				$page=1;
            }else
                $offset = ($page-1) * $limit;
			$pagecount = ceil($c/$limit);

			if ($pagecount ==0) $pagecount=1;

			if(!empty($where)) $this->thread->where(sprintf("`show` = 1 and `%s` = '%s'",$type,$where));
			if(!empty($where)) $this->_view->assign('tag',$where);
			$thread_result = $this->thread->limit("{$offset},{$limit}")->orderby('clicknum desc,pubdate desc')->select();
			$this->_view->assign('thread',$thread_result);
			$this->_view->assign('page',array('page'=>$page,'pagecount'=>$pagecount));
			$this->_view->display("tiezi/{$tpl}.htm");
		}catch(Exception $e){
			print_r($e);
		}
	}
}