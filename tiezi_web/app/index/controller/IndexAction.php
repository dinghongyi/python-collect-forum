<?php
mb_internal_encoding("GBK");
import('Html','','');
import('Page','','');
import('CommonAction','','');
class Index_Action extends Common_Action{
	public function init(){
		parent::init();
		$this->hottop();
		$this->smalltype();

		//头条一
		$this->typeeight("FIND_IN_SET('b', flag)>0 and FIND_IN_SET('h', flag)>0",'tou','pubdate desc','0,1','subject,id','find');
		//头条二
		#$this->typeeight("FIND_IN_SET('h', flag)>0 and type=1",'tou1','pubdate desc','0,1','subject,id','find');
		//头条三
		#$this->typeeight("FIND_IN_SET('h', flag)>0 and type=2",'tou2','pubdate desc','0,1','subject,id','find');

		//幻灯
		$this->typeeight("FIND_IN_SET('p', flag)>0 and FIND_IN_SET('f', flag)>0",'huan','pubdate desc','0,5','subject,id,litpic');

		//第一版 搞笑  原创 小白
		$this->typeeight("tid = 1 and FIND_IN_SET('p', flag)=0 ",'t1','pubdate desc','0,4','subject,id','select','1',600); //情感
		$this->typeeight("tid = 3 and FIND_IN_SET('p', flag)=0 ",'t3','pubdate desc','0,4','subject,id','select','1',600); //女性
		$this->typeeight("tid = 4 and FIND_IN_SET('p', flag)=0 ",'t4','pubdate desc','0,4','subject,id','select','1',600);//原创
		$this->typeeight("tid = 152 and FIND_IN_SET('p', flag)=0 ",'t5','pubdate desc','0,4','subject,id','select','1',600);//小白


		//高校梅图
		$this->typeeight("FIND_IN_SET('f', flag)=0 and FIND_IN_SET('h', flag)=0 and FIND_IN_SET('s', flag)=0 and FIND_IN_SET('b', flag)=0 and FIND_IN_SET('c', flag)=0 and FIND_IN_SET('p', flag)>0 ",'img','pubdate desc','0,7','subject,id,litpic','select');

		$this->tt();
		$this->dzh();
	}
	private function tt(){
		//贴贴头三条
		$this->typeeight(wheretime('pubdate','curdate')." and FIND_IN_SET('p', flag)=0",'tth3','replaynum  desc','0,3','subject,id','select');
		//贴贴 二图
		$this->typeeight(wheretime('pubdate','curmonth')." and FIND_IN_SET('p', flag)>0 ",'img1','clicknum desc','0,1','subject,id,litpic','find');
		$this->typeeight(wheretime('pubdate','curdate')." and FIND_IN_SET('p', flag)>0 ",'img2','clicknum desc','0,1','subject,id,litpic','find');

		//第二版 8 条信息
		$this->typeeight(" FIND_IN_SET('p', flag)>0 and tid=191",'tt','pubdate desc','0,8');
		$this->typeeight(" FIND_IN_SET('p', flag)>0 and tid=191",'tt1','pubdate desc','9,8');
		//SELECT * FROM `ti_thread` WHERE body REGEXP '<IMG'

		//每日排行 每月排行FIND_IN_SET('b', flag)>0 and
		$this->typeeight(wheretime('pubdate','curdate'),'paihang','clicknum desc','0,6','subject,id,clicknum');
		$this->typeeight(wheretime('pubdate','curmonth'),'paihang1','clicknum desc','0,6','subject,id,clicknum');
		//最近更新
		$this->typeeight("FIND_IN_SET('p', flag)=0 and FIND_IN_SET('c', flag)=0 and FIND_IN_SET('h', flag)=0 and FIND_IN_SET('b', flag)=0 ",'id1','id desc','0,7','subject,id,dateline');
	}
	private function dzh(){
		//贴贴头三条
		$this->typeeight("FIND_IN_SET('c', flag)>0 and FIND_IN_SET('p', flag)=0 ",'dtth3','pubdate desc','0,3','subject,id','select');
		//贴贴 二图
		$this->typeeight("FIND_IN_SET('s', flag)>0 and FIND_IN_SET('p', flag)>0 ",'dimg1','pubdate desc','0,1','subject,id,litpic','find');
		$this->typeeight("FIND_IN_SET('b', flag)>0 and FIND_IN_SET('p', flag)>0 ",'dimg2','pubdate desc','1,1','subject,id,litpic','find');

		//第二版 8 条信息
		$this->typeeight("FIND_IN_SET('p', flag)=0 ",'dtt','id desc','30,8');
		$this->typeeight("FIND_IN_SET('p', flag)=0 ",'dtt1','pubdate desc','40,8');
		//SELECT * FROM `ti_thread` WHERE body REGEXP '<IMG'

		//每日排行 每月排行
		$this->typeeight("FIND_IN_SET('b', flag)>0 ",'dpaihang','clicknum desc','0,6','subject,id,clicknum');
		$this->typeeight("FIND_IN_SET('c', flag)>0 ",'dpaihang1','clicknum desc','0,6','subject,id,clicknum');
		//最近更新
		$this->typeeight("FIND_IN_SET('p', flag)=0 and FIND_IN_SET('c', flag)=0 and FIND_IN_SET('h', flag)=0 and FIND_IN_SET('b', flag)=0 ",'did1','id desc','0,7','subject,id,dateline');
	}
	public function index(){
		header('X-dinghongyiFrameWork-index: test');
		try{
			$this->_view->display('tiezi/index.htm');
		}catch(Exception $e){
			print_r($e);
		}
	}
	private function hottop(){

		if(!$this->redis->exists('hottop')){
			/* `show` = 1 and */
			//SELECT * FROM ti_thread WHERE DATE_FORMAT(FROM_UNIXTIME(`dateline`),'%Y%m')=DATE_FORMAT(CURDATE(),'%Y%m')
			$result = $this->thread->where(wheretime('pubdate','curmonth')." and `show`=1")->fields('subject,id')->limit('0,8')->orderby('u desc')->select();
			$this->redis->set('hottop',serialize($result),1800);
		}else{
			$result = $this->redis->get('hottop');
			$result = unserialize($result);
		}
		$this->_view->assign('hottop',$result);
	}
	private function smalltype(){
		$typearr = array(
			1 => array('文学'=>73,'时尚女性'=>74,'图片'=>94,'龙门茶社'=>87,'情感'=>68,'图说时事'=>69,),
			2 => array('大小姐'=>96,'小白区'=>100,'原创区'=>99,'汽车烩'=>97,'贴图区'=>98,),
			);
		$this->_view->assign('tid',$typearr);
	}

	private function typeeight($typeid,$view,$order = 'id desc',$limit = '0,8',$fields='subject,id',$t='select',$show='1',$lifetime=1800){
		if(is_int($typeid)){
			$typeid = sprintf("tid = %d",$typeid);
		}else{
			$typeid = $typeid;
		}
		$typeid .=" and `show`=$show";
		 $order=' '. $order;

		if($_GET['upcache'] ==1)
			$this->redis->flushdb();
		$rid = md5($typeid.$view.$order.$limit.$fields.$t.$show.$lifetime);
		if(!$this->redis->exists($rid)){
			if($t == 'find')
				$result = $this->thread->fields($fields)->orderby($order)->where($typeid)->find();
			elseif($t == 'select')
				$result = $this->thread->fields($fields)->limit($limit)->orderby($order)->where($typeid)->select();
			$this->redis->set($rid,serialize($result),$lifetime);
		}else{
			$result = $this->redis->get($rid);
			$result = unserialize($result);
		}
		$this->_view->assign($view,$result);
	}
	public function feed(){
		header("Content-type: application/xml; charset=GBK");
		$rid = 'index_feed';
		if(!$this->redis->exists($rid)){
			$thread = new ModelDb('thread');
			foreach($thread->orderby('id desc')->limit('0,20')->select() as $v){
				$t = array();
				$t['url'] = sprintf('http://www.520gb.com/postpage/%s/',$v['id']);
				$t['subject'] = $v['subject'];
				$t['pubdate'] = date('c',$v['pubdate']);
				$t['dateline'] = date('c',$v['dateline']);
				$t['name'] = $v['name'];
				$t['uri'] = sprintf('http://www.520gb.com/grouplist/name/?%s',urlencode(strip_tags($t['name'])));
				$v['body'] = replace_html($v['body'],strip_tags($t['subject']));
				$t['descript'] = mb_substr(strip_tags($v['body']),0,200);
				$t['body'] = $v['body'];
				$result[] = $t;
			}
			$this->redis->set($rid,serialize($result),600);
		}else{
			$result = unserialize($this->redis->get($rid));
		}
		$this->_view->assign('time',date('c'));
		$this->_view->assign('row',$result);
		$this->_view->display('tiezi/feed.htm');
	}
	public function baidusitemap(){
		//header("Content-type: text/plain; charset=GBK");
		$rid = 'index_baidusitemap';
		if(!$this->redis->exists($rid)){
			$thread = new ModelDb('thread');
			foreach($thread->orderby('id desc')->limit('0,400')->select() as $v){
				$t = array();
				$t['url'] = sprintf('http://www.520gb.com/postpage/%s/',$v['id']);
				$t['subject'] = $v['subject'];
				#$t['pubdate'] = date('c',$v['pubdate']);
				#$t['dateline'] = date('c',$v['dateline']);
				#$t['name'] = $v['name'];
				#$t['uri'] = sprintf('http://www.520gb.com/grouplist/name/?%s',urlencode(strip_tags($t['name'])));
				#$v['body'] = replace_html($v['body'],strip_tags($t['subject']));
				#$t['descript'] = mb_substr(strip_tags($v['body']),0,200);
				#$t['body'] = $v['body'];
				$result[] = $t;
			}
			$this->redis->set($rid,serialize($result),600);
		}else{
			$result = unserialize($this->redis->get($rid));
		}
		foreach($result as $v){
			echo "<a href='{$v['url']}' title='{$v['subject']}'>{$v['subject']}</a>\n";
		}
	}
	public function sitename(){
		//header("Content-type: text/plain; charset=GBK");
		$rid = 'index_sitename';
		if(!$this->redis->exists($rid)){
			$thread = new ModelDb('thread');
			foreach($thread->orderby('id desc')->groupby('name')->limit('0,500')->select() as $v){
				$t = array();
				$t['name'] = $v['name'];
				$t['nameurl'] = sprintf('http://www.520gb.com/grouplist/name/?%s',urlencode($v['name']));
				#$t['subject'] = $v['subject'];
				#$t['pubdate'] = date('c',$v['pubdate']);
				#$t['dateline'] = date('c',$v['dateline']);
				#$t['name'] = $v['name'];
				#$t['uri'] = sprintf('http://www.520gb.com/grouplist/name/?%s',urlencode(strip_tags($t['name'])));
				#$v['body'] = replace_html($v['body'],strip_tags($t['subject']));
				#$t['descript'] = mb_substr(strip_tags($v['body']),0,200);
				#$t['body'] = $v['body'];
				$result[] = $t;
			}
			$this->redis->set($rid,serialize($result),86400);
		}else{
			$result = unserialize($this->redis->get($rid));
		}
		foreach($result as $v){
			echo "<a href='{$v['nameurl']}' title='{$v['name']}'>{$v['name']}</a>\n";
		}
	}
}