<?php
import('Page','','');
import('sphinxapi','','');
class Search_Action extends Action{
	public function init(){
		$this->redis = new Redis();
		$this->redis->connect('127.0.0.1', 6379);
		$this->redis->select($this->_config['redisdb']);
		$this->cl = new SphinxClient ();
		$this->cl->SetServer ( '127.0.0.1', 9312);
		$this->cl->SetConnectTimeout ( 3 );

		$this->thread = new ModelDb('thread');
	}
	public function index(){
		try{
			$q = $_GET['q'];
			//
			if(mb_strlen($q) > 30){
				throw new Exception('字数不能超过30个！');
			}
			if(mb_strlen($q) < 1){
				throw new Exception('哈哈');
				//$this->_view->assign('');
			}

			$page = $_GET['page'];
			if(!preg_match('/\d+/',$page)){
				$page = 1;
			}
			$limit = $this->_config['list_limit'];
			if(empty($page) or $page == 1){
				$offset = 0;
				$page=1;
            }else
                $offset = ($page-1) * $limit;
			$this->cl->SetLimits($offset,$limit);
			$this->cl->SetArrayResult ( true );
			$this->cl->SetMatchMode ( SPH_MATCH_ANY);

			$rq = "$q";
			$res = $this->cl->Query ( $rq, "mysql" );
			if(!$res['matches']){
				throw new Exception('出错');
			}

			 $ids = implode(',',array_map('current',$res['matches']));
			//exit();SELECT * FROM 数据表 WHERE ID IN(id1,id2,id3...,idn) ORDER BY FIND_IN_SET(ID,"id1,id2,id3...,idn")
			$rid = md5($rq);
			if(!$this->redis->exists($rid)){
				$this->redis->set($rid,$res['total'],3600);
				$c = $res['total'];
			}else{
				$c = $this->redis->get($rid);
			}
			$pagecount = ceil($c/$limit);
			if ($pagecount ==0) $pagecount=1;

			$this->thread->fields('id,pubdate,subject,body,tid,tid2')->where("id IN({$ids})");
			$result = $this->thread->orderby("FIND_IN_SET(ID,'{$ids}') desc")->select();
			//print_r($this->thread);
			foreach($result as &$v){

				$v['body'] = mb_substr(trim(strip_tags($v['body'])),0,80,'UTF-8');
				foreach(array_keys($res['words']) as $a){
					$v['body'] = str_replace($a,'<font color="red">'.$a.'</font>',$v['body']);
					$v['subject'] =str_replace($a,'<font color="red">'.strip_tags($a).'</font>',$v['subject']);
					$v['name'] = str_replace($a,'<font color="red">'.$a.'</font>',$v['name']);
				}

			}
			$this->_view->assign('slist',$result);
			$this->_view->assign('q',rawurlencode($q));
			$this->_view->assign('page',array('page'=>$page,'pagecount'=>$pagecount));
			$this->_view->display('tiezi/search.htm');
		}catch(Expcetion $e){
			print_r($e);
		}
		$this->search();
	}
	public function search(){

	}
}