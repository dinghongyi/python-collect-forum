<?php

class Api_Action extends Action{
	public function typeall(){
		$type = new ModelDb('type');
		foreach($type->fields('id,name')->where('pid > 0 ')->select() as $v){
			$data[] = array($v['id'],mb_convert_encoding($v['name'],'utf-8','gbk'));
		}
		echo json_encode($data);
	}
	public function thread_add(){
		if(!$_POST){ echo json_encode(array('status'=> 'no','info'=>'only method for post'));return;}
		#insert into ti_thread (`tid2`,`tid`,`subject`,`dateline`,`name`,`pubdate`,`flag`,`body`,`clicknum`,`replaynum`,`url`,`litpic`)
		$thread = new ModelDb('thread');
		$threads = new ModelDb('threads');
		$data = array(
				'tid' => $_POST['tid'],
				'tid2' => $_POST['tid2'],
				'subject' =>$_POST['subject'],
				'dateline' => time(),
				'name' => $_POST['name'],
				'pubdate' => time(),
				'flag' => $_POST['flag'],
				'clicknum' => $_POST['clicknum'],
				'replaynum' => $_POST['replaynum'],
				'url' => $_POST['url'],
				'litpic' => $_POST['litpic'],
				//'body' => $_POST['body'],
			);
		$lastid = $thread->add($data);
		$data1 = array(
			'aid' =>$lastid,
			'tid' =>$_POST['tid'] ,
			'tid2' =>$_POST['tid2'] ,
			'name' =>$_POST['name'] ,
			'dateline' => time(),
			'body' => $_POST['body'],
			);
		$threads->add($data1);
		echo json_encode(array ('lastid' => $lastid));
	}
	public function thread_u_update(){
		if(!$_POST){ echo json_encode(array('status'=> 'no','info'=>'only method for post'));return;}
		$thread = new ModelDb('thread');
		$data = array(
				'u' => $_POST['u'],
				//'orde' => $_POST['orde'],
				//'pubdate'=>'',
			);
		$thread->where(sprintf('id = %d',$_POST['id']))->update($data);
	}
	public function threads_add(){
		if(!$_POST){ echo json_encode(array('status'=> 'no','info'=>'only method for post'));return;}
		#insert into ti_threads (`tid`,`dateline`,`body`,`type`,`ttype`,`name`)
		$threads = new ModelDb('threads');
		$data = array(
				'aid' => $_POST['aid'],
				'dateline' => time(),
				'body' =>$_POST['body'],
				'tid' => $_POST['tid'],
				'tid2' => $_POST['tid2'],
				'name' => $_POST['name']
			);
	}
	public function thread_update(){
		if(!$_POST){ echo json_encode(array('status'=> 'no','info'=>'only method for post'));return;}
		#ti_thread set `purl`='%s' ,`orde`=%s where id=%s
		$thread = new ModelDb('thread');
		$data = array(
				'purl' => $_POST['purl'],
				'orde' => $_POST['orde'],
				//'pubdate'=>'',
			);
		if($_GET['update']) $data['pubdate'] = time();
		$thread->where(sprintf('id = %d',$_POST['id']))->update($data);
	}
	public function gettypepage(){
		$thread =new ModelDb('thread');
		$tid = $_GET['tid'];

		#echo '<root>';
		foreach($thread->fields('id,url,purl,u,orde,name')->where(sprintf("tid = %s and `show`=0 and `purl` != '' ",$tid))->select() as $v){
			#echo '<item>';
			#echo '<id><![CDATA[' . $v['id'] . ']]></id>';
			#echo '<url><![CDATA[' . $v['url'] . ']]></url>';
			#echo '<purl><![CDATA[' . $v['purl'] . ']]></purl>';
			#echo '<u><![CDATA[' . $v['u'] . ']]></u>';
			#echo '<orde><![CDATA[' . $v['orde'] . ']]></orde>';
			#echo '<name><![CDATA[' . strip_tags($v['name']) . ']]></name>';
			#echo '</item>';
			$list[] = array($v['id'],$v['url'],$v['purl'],$v['u'],$v['orde'],mb_convert_encoding(strip_tags($v['name']),'utf-8','gbk'));
		}
		#echo '</root>';

		#foreach($thread->fields('id,url,purl,u,orde,name')->where(sprintf("tid = %s and `show`=1 and `purl` != '' and `pubdate` < ( UNIX_TIMESTAMP( ) -86400 *7 )",$tid))->select() as $v){
		#	$list[] = array($v['id'],$v['url'],$v['purl'],$v['u'],$v['orde'],mb_convert_encoding($v['name'],'utf-8','gbk'));
		#}
		//$list = $thread->fields('id,url,purl,u,orde,name')->where(sprintf("tid = %s and `show`=1 and `purl` != '' and `pubdate` < ( UNIX_TIMESTAMP( ) -86400 *7 )",$tid))->select();

		echo json_encode($list);
	}
}
