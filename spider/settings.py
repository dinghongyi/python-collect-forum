#!/usr/bin/env python
#coding:utf-8
#author:dingyangfan
#url:http://www.meiyear.com

timeout = 15  #下载超时
thread_count = 10  #下载线程
time_s = 1 #下载速度间隔 时间单位
mysqlinfo = ('127.0.0.1','root','zyqdudumysql','tiezi','gbk',3307)
redisinfo = ('127.0.0.1',6379,1)

run_time = 1800 #单个进程 运行时间
step = 1 #默认 分页跳的参数
startvpage = 2 #默认分页下载处
insertcharset='gbk'
dateformat='%Y-%m-%d %H:%M:%S'
headers = {
    'User-Agent':'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/534.30 (KHTML, like Gecko) Chrome/12.0.742.9 Safari/534.30',\
    'Referer':'http://www.mop.com'\
}

ding_rule = [
    { 'sid':1, #唯一id
      'f':'tt_mop_com', #信息输出标识
      'pagechatset' : 'gbk',
      'typen' : 1, #分类id
      'urlpage' : r'/read_(.*)_1_0\.html',
      'page' : 'http://tt.mop.com/read_%s_1_1.html',
      'vpage' : 'http://tt.mop.com/read_%s_%s_1.html',
      'urls' : [
          'http://tt.mop.com/', #首页
          'http://tt.mop.com/topic/list_1_0_1_0.html',
          'http://tt.mop.com/topic/list_209_0_0_0.html',
          'http://tt.mop.com/topic/list_76_0_0_0.html',
		  'http://tt.mop.com/topic/list_76_78_0_0.html',
		  'http://tt.mop.com/topic/list_70_73_0_1_1.html',
		  'http://tt.mop.com/topic/list_213_17_0_0.html',
		  'http://tt.mop.com/topic/list_94_0_1_1_1.html',
		  'http://tt.mop.com/topic/list_209_50_0_0.html',
		  'http://tt.mop.com/topic/list_209_357_0_0.html',
          ],# + ['http://tt.mop.com/topic/list_209_50_0_%s_1.html'%i for i in range(100,0,-1)],
      'unkownpagetag' : [u'该贴已经被删除',u'该贴未通过审核'],
      'titletag' : r'<h1 title="(.*?)" id="js-title">(.*?)</h1>',
      'clicktag' : u'class="fcR">(\d+)</span>点击',
      'replaytag' : u'class="fcR">(\d+)</span>回复',
      'nametag' : r'<a target="_blank" href="/user/(.*?)" class="fcB">(.*?)</a>',
      'ttypetag' : r"<a  href='/topic/list_(.*?)_0_0_1.html' target='_blank'>(.*?)</a>&gt;",
      'datelinetag' : u'<span class="date">发表时间：(.*?)</span>',
      'bodytag' : r'%s(.*?)%s'%('<div class="tz_mainP" id="js-sub-body">','<ul class="tz_yhxxP">'),
      'endpagetag' : u"class='endgray'>尾页</a>",
      'pagecounttag' : """re.compile(r"<a href='/read_(\d+)_(\d+)_1\.html' class='endgray'>",re.DOTALL).findall(content)[0][1]""",
      'commenttimestag' :"""re.compile(u'<div class="h_lz">(.*?)发表</div>',re.DOTALL).findall(content)""",
      'commentbody' : """re.compile(r'<div class="h_nr js-reply-body">(.*?)</div>',re.DOTALL).findall(content)""",
      'startvpage': 2,
      'run_time':600,
      },
    {
        'headers':{},
        'sid':2, #唯一id
        'f':'dzh_mop_com', #信息输出标识
        'pagechatset' : 'utf-8',
        'typen' : 2, #分类id
        'urlpage' : r'/(.*)\.shtml',
        'page' : 'http:/%s.shtml?only=1&dzhrefer=true',
        'vpage' : 'http:/%s.shtml?only=1&dzhrefer=true',
        'urls' : [
            'http://dzh.mop.com',
            'http://dzh.mop.com/book',
            'http://dzh.mop.com/newbie',
            'http://dzh.mop.com/ladyclub',
			'http://dzh.mop.com/picarea',
            ],
        'unkownpagetag' : [u'该贴已经被删除',u'该贴未通过审核'],
        'titletag' : r'id="title">(.*?)</span>',
        'clicktag' : u'浏览量：(\d+)　回复数',
        'replaytag' : u'回复数：(\d+)[\n\t\r]+&nbsp;',
        'nametag' : r'<a href="/user/ta.do\?userId=(.*?)" target="_blank">(.*?)</a>',
        'ttypetag' : r'&gt; <a href="(.*?)" target="_blank">(.*?)</a> &gt;',
        'datelinetag' : r'<li class="tzrq">(.*?)</li>',
        'bodytag' : r'%s(.*?)%s'%('<div class="tznrP" id="body">','<div id="lzxx_fun" class="lzxx">'),
        'endpagetag' : u"'>尾页</a>",
        'pagecounttag' :  """re.compile(r"<a class='endgray' href='/(\w+)/(\d+)/(\d+)/(\w+).shtml\?only=1&dzhrefer=true'>",re.DOTALL).findall(content)[0][2]""",
        'commenttimestag' :"""re.compile(r'<li class="operate">(.*?)</li>',re.DOTALL).findall(content)""",
        'commentbody' : """re.compile(r'<li class="details">(.*?)</li>',re.DOTALL).findall(content)""",
        'startvpage':1,
        'pagepid':[('/0/','/%s/')],
        'run_time':600,
        },
    {

        'sid':3, #唯一id
        'f':'www_douban_com', #信息输出标识
        'pagechatset' : 'utf-8',
        'typen' : 3, #分类id
        'urlpage' : r'http://www.douban.com/group/topic/(\d+)/',
        'page' : 'http://www.douban.com/group/topic/%s/',
        'vpage' : 'http://www.douban.com/group/topic/%s/?start=%s',
        'urls' : [
            'http://www.douban.com/group/tupian/discussion?start=0',
            'http://www.douban.com/group/movieworld/discussion?start=0',
            'http://www.douban.com/group/yly/discussion?start=0',
            'http://www.douban.com/group/52725/discussion?start=0',
            'http://www.douban.com/group/chen19891018/discussion?start=0',
			'http://www.douban.com/group/keeven/discussion?start=0',
			'http://www.douban.com/group/408652/discussion?start=0',
			'http://www.douban.com/group/259706/discussion?start=0',
			'http://www.douban.com/group/Singlestill/discussion?start=0'
            ],
        'unkownpagetag' : [u'该贴已经被删除',u'该贴未通过审核'],
        'titletag' : r'<h1>(.*?)</h1>',

        'nametag' : u'来自: <a href="http://www.douban.com/people/(.*?)/">(.*?)</a>',
        'ttypetag' : u'<a href="http://www.douban.com/group/(.*?)/\?ref=1">(.*?)小组</a>',
        'datelinetag' : r'<span class="color-green">(.*?)</span>',
        'bodytag' : r'%s(.*?)%s'%('<div class="topic-content">','<div class="sns-bar" id="sep">'),
        'endpagetag' : u"后页&gt;</a>",
        'pagecounttag' :  """re.compile(r'<a href="http://www.douban.com/group/topic/(\d+)/\?start=(\d+)" >(\d+)</a>',re.DOTALL).findall(content)[-1][1]""",
        'commenttag':["""//ul[@id="comments"]/li[@class="clearfix comment-item"]""",\
                      {
                          'time':"""div[@class="reply-doc content"]/div[@class="bg-img-green"]/h4/span[@class="pubtime"]""",\
                          'name':"""div[@class="reply-doc content"]/div[@class="bg-img-green"]/h4/a""",\
                          'body': 'div[@class="reply-doc content"]/p'
                      }],
        'startvpage':100,
        'step':100,
        'time_s':10,
        'thread_count':2,
        'run_time':1800,
		'onlyname':1,
       
    },
    {
        'headers':{},
        'sid':4,
        'f':'tieba_baidu_com',
        'pagechatset' : 'gbk',
        'typen' : 4, #分类id
        'urlpage' : r'/p/(\d+)',
        'page' : 'http://tieba.baidu.com/p/%s?see_lz=1',
        'vpage' : 'http://tieba.baidu.com/p/%s?see_lz=1&pn=%s',
        'urls' : [
            'http://tieba.baidu.com/f?kw=%B9%ED',
            'http://tieba.baidu.com/f?kw=%BA%F3%B9%AC%B6%AF%C2%FE&fr=itb_favo&fp=favo',
            'http://tieba.baidu.com/f?kw=%D0%A6%BB%B0',
            'http://tieba.baidu.com/f?kw=%D3%E9%C0%D6',
            'http://tieba.baidu.com/f?kw=%C3%C0%C5%AE',
			'http://tieba.baidu.com/f?kw=%D3%D0%C8%A4&tp=0&pn=0',
            ] ,#+ ['http://tieba.baidu.com/f?kw=%D3%D0%C8%A4&tp=0&pn=' + str(i) for i in range(1234,0,-1)],
        'unkownpagetag' : [u'该贴已经被删除',u'该贴未通过审核'],
        'titletag' : r'title:"(.*?)"',

        'nametag' : r'''<div class="l_post noborder" data-field='{"author":{"id":(\d+),"name":"(.*?)","name_u"''',
        'ttypetag' : u'<a href="/f\?kw=(.*?)" title="(.*?)"><<返回',
        'datelinetag' : r'"date":"(.*?)"',
        'bodytag' : r'%s(.*?)%s'%('<div class="louzhubiaoshi_wrap">','<div class="core_reply j_lzl_wrapper">'),
        'endpagetag' : u"尾页</a>",
        'pagecounttag' :  """re.compile(r'<a href="/p/(\d+)\?see_lz=1&pn=(\d+)">',re.DOTALL).findall(content)[-1][1]""",
        'commenttag':["""//div[@class="l_post "]""",\
                      {
                          #'time':'text()',
                          'body': """div[@class="d_post_content_main "]/div[@class="p_content"]/cc/div"""
                      }],
        'startvpage':2,
        'step':1,
        'time_s':1,
        'thread_count':3,
        'run_time':900,
        'dateformat':'%Y-%m-%d %H:%M'
    },

    {
	'headers' : {},
         'sid':152,
         'f':'bbs_tianya_cn',
         'pagechatset' : 'utf-8',
         'typen' : 152, #分类id
         'urlpage' : r'/post-(.*?)-1.shtml',
         'page' : 'http://bbs.tianya.cn/post-%s-1.shtml',
         'vpage' : 'http://bbs.tianya.cn/post-%s-%s.shtml',
         'urls' : [
             'http://bbs.tianya.cn/list-170-1.shtml',
			 'http://bbs.tianya.cn/list-free-1.shtml',
			 'http://bbs.tianya.cn/list-feeling-1.shtml',
             'http://bbs.tianya.cn/list-shortmessage-1.shtml',
             'http://bbs.tianya.cn/list-develop-1.shtml',
             'http://bbs.tianya.cn/list-no11-1.shtml',
             'http://bbs.tianya.cn/list-402-1.shtml',
             'http://bbs.tianya.cn/list-730-1.shtml',
             'http://bbs.tianya.cn/list-981-1.shtml',
             'http://bbs.tianya.cn/list-12-1.shtml',
             'http://bbs.tianya.cn/list-no05-1.shtml',
             'http://bbs.tianya.cn/list-books-1.shtml',
             'http://bbs.tianya.cn/list-39-1.shtml',
             'http://bbs.tianya.cn/list-41-1.shtml',
             'http://bbs.tianya.cn/list-44-1.shtml',
             ] ,
         'unkownpagetag' : [u'该贴已经被删除',u'该贴未通过审核'],
         'titletag' : r"""<span class="s_title"><span style="font-weight:400;">(.*?)</span></span>""",
         'nametag' : r'''target="_blank" class="js-vip-check" uid="(\d+)" uname="(.*?)"''',
         'ttypetag' : r'<em><a href="/list-(.*?)-1.shtml">(.*?)</a>',
         'datelinetag' : u'<span>时间：(.*?) </span>',
         'clicktag' : u'<span>点击：(\d+) </span>',
         'replaytag' : u'<span>回复：(\d+)</span>',
         'bodytag1':'div.bbs-content.clearfix',
         'endpagetag' : r'class="js-keyboard-next"',
         'pagecounttag' :  """re.compile(r'onsubmit="return goPage\(this,(.*?),(\d+)\);"',re.DOTALL).findall(content)[-1][1]""",
         'commenttag':["""//div[@class="atl-item"]""",\
                       {
                           'name':"""div[@class="atl-head"]/div[@class="atl-info"]/span/a""",
                          # 'time':"""div[@class="atl-head"]/div[@class="atl-info"]/span[2]""",
                           'body': """div[@class="atl-content"]/div[@class="atl-con-bd clearfix"]/div[@class="bbs-content"]"""
                       }],
         'startvpage':2,
         'step':1,
         'time_s':5,
         'thread_count':3,
         'run_time':900,
		 'onlyname':1,
        # 'dateformat':'%Y-%m-%d %H:%M:%S'
     },
    {
         'sid':191,
         'f':'bbs_voc_com_cn',
         'pagechatset' : 'gb2312',
         'typen' : 191, #分类id
         'urlpage' : r'topic-(.*?)-1-1.html',
         'page' : 'http://bbs.voc.com.cn/topic-%s-1-1.html',
         'vpage' : 'http://bbs.voc.com.cn/topic-%s-%s-1.html',
         'urls' : [
            'http://bbs.voc.com.cn/forum-50-1.html',
             ] ,
         'unkownpagetag' : [u'该贴已经被删除',u'该贴未通过审核'],
         'titletag' : r"""addbookmark\(window.location,'(.*?)'\)""",
         'nametag' : u'''<span style="(.*?)"><font color="gray">(.*?) 发表在</font>''',
         'ttypetag' : u'发表在</font> <a href="forum-(.*?).html">(.*?)</a>',
         'datelinetag' : r',(.*?),2222,0--></div>',
         'clicktag' : u'共<b>(\d+)</b>个阅读者',
         'replaytag' : u'<b>(\d+)</b>条回复',
         'bodytag1':"div.t_msgfont.BSHARE_POP.BSHARE_IMAGE_CLASS",
         'endpagetag' : u'下一页&raquo;</a>',
         'pagecounttag' :  """re.compile(r'<a class="p_pages">&nbsp;(\d+)/(\d+)&nbsp;</a>',re.DOTALL).findall(content)[-1][1]""",
         'commenttag':[u"""//div[@class="spaceborder" and @style="width:97%;margin-bottom:0px;"]/table[@class='t_row']""",\
                       {
                           'name':"""tr[0]/td[1]/table[0]/tr/td/div/a[@style]""",
                           # 'time':"""div[@class="atl-head"]/div[@class="atl-info"]/span[2]""",
                           'body': """tr[1]/td[2]/table[1]/tr[2]/td[1]/div[@class="t_msgfont BSHARE_POP BSHARE_IMAGE_CLASS"]"""
                       }],
         'startvpage':2,
         'step':1,
         'time_s':1,
         'thread_count':3,
         'run_time':900,
		 'onlyname':1,
         'dateformat':'%Y-%m-%d %H:%M',
         'recontent':[('</table></table><img','<img')],
         'rebody':[(u'更多纯美小清新、可爱萌萝莉、大尺度私房秀、性感诱人女优、欧美御姐风情，请戳美女贴图新版首页：',''),\
                   ('http://mm.voc.com.cn/',''),\
                   ('onload="if(this.width>screen.width*0.7) this.width=screen.width*0.7;"',''),\
                   ('onmouseover="if(this.width>screen.width*0.7) this.width=screen.width*0.7;"',''),\
                   ('onmousewheel="return imgzoom(this);"',''),\
                   (u'alt="按此在新窗口浏览图片"',''),\
                   ]
     },
]