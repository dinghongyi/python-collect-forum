#coding=utf-8
import settings
import MySQLdb,threading
import time

def show():
    conn = MySQLdb.Connect(host=settings.mysqlinfo[0],user=settings.mysqlinfo[1],passwd=settings.mysqlinfo[2],db=settings.mysqlinfo[3],charset=settings.mysqlinfo[4],port=settings.mysqlinfo[5])
    c = conn.cursor()
    c.execute("""SELECT id
FROM  `ti_thread`
WHERE  `show` =0
GROUP BY tid2
""")
    result = c.fetchall()
    print result
    #c.executemany("update `ti_thread` set `show`=1 ,`pubdate`=%s"%int(time.time())+" where id =%s",result)
    c.execute("update `ti_thread` set `show`=1 ,`pubdate`=%s"%int(time.time())+" where `show` =0")
    c.close()
    conn.close()
    threading.Timer(600,show).start()
if __name__ == '__main__':
    show()