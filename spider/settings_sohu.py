#!/usr/bin/env python
#coding:utf-8
#author:dingyangfan
#url:http://www.meiyear.com
from settings import *
__ding_rule ={
        'pagechatset' : 'utf-8',
        'typen' : 151, #分类id
        'unkownpagetag' : [u'该贴已经被删除',u'该贴未通过审核'],
        'titletag' : r"""<h1>(.*?)</h1>""",
        'nametag' : r'''<a class="nickname" href="http://club.sohu.com/read_user.php\?userCN=(.*?)" target="_black">(.*?)</a>''',
        'ttypetag' : u"</em>&gt;<a href='http://(.*?)/'>(.*?)</a></div>",
        'datelinetag' : u"<em class='ctime' rel='(.*?)'></em>",
        'clicktag' : u'阅读:(\d+)&nbsp;',
        'replaytag' : u'回复:(\d+)',
        'bodytag' : r'''<div class="viewModes">(.*?)<div class="answer clear">''',
        'endpagetag' : u'class="nextPage',
        'pagecounttag' :  """re.compile(r'<em class="total">(\d+)</em>',re.DOTALL).findall(content)[-1]""",
        'commenttag':["""//div[@class="content contentRe clear"]""",\
                      {
                          'name':"""div[@class="user left"]/div[@class="portrait"]/a[@class="nickname"]""",
                         # 'time':"""div[@class="atl-head"]/div[@class="atl-info"]/span[2]""",
                          'body': """div[@class="left threadright"]/div[@class="txt"]"""
                      }],
        'startvpage':1,
        'step':1,
        'time_s':5,
        'thread_count':3,
        'run_time':900,
        'onlyname':1,
        'timet':1,
       # 'dateformat':'%Y-%m-%d %H:%M:%S'
    }
ding_rule = [
    {
        'sid':1,
        'f':'literature_suhu_com',
        'urlpage' : r'/literature/thread/(.*?)/',
        'page' : 'http://club.cul.sohu.com/literature/thread/%s/',
        'vpage' : 'http://club.cul.sohu.com/literature/thread/%s/?author=all&trash=0&page=%s',
        'urls' : [
            'http://club.cul.sohu.com/literature/threads/',
            ] ,
    },
    {
        'sid':2,
        'f':'xuanhuan_suhu_com',
        'urlpage' : r'/xuanhuan/thread/(.*?)/',
        'page' : 'http://club.cul.sohu.com/xuanhuan/thread/%s/',
        'vpage' : 'http://club.cul.sohu.com/xuanhuan/thread/%s/?author=all&trash=0&page=%s',
        'urls' : [
            'http://club.cul.sohu.com/xuanhuan/threads/#p1',
            ] ,
    },
    {
        'sid':3,
        'f':'bagua_suhu_com',
        'urlpage' : r'/bagua/thread/(.*?)/',
        'page' : 'http://club.yule.sohu.com/bagua/thread/%s/',
        'vpage' : 'http://club.yule.sohu.com/bagua/thread/%s/?author=all&trash=0&page=%s',
        'urls' : [
            'http://club.yule.sohu.com/bagua/threads/#p1',
            ] ,
    },
             {
                 'sid':4,
                 'f':'zz0220_suhu_com',
                 'urlpage' : r'/zz0220/thread/(.*?)/',
                 'page' : 'http://club.yule.sohu.com/zz0220/thread/%s/',
                 'vpage' : 'http://club.yule.sohu.com/zz0220/thread/%s/?author=all&trash=0&page=%s',
                 'urls' : [
                     'http://club.yule.sohu.com/zz0220/threads/#p1',
                     ] ,
     },
      {
                 'sid':5,
                 'f':'fun_pics_suhu_com',
                 'urlpage' : r'/fun_pics/thread/(.*?)/',
                 'page' : 'http://club.yule.sohu.com/fun_pics/thread/%s/',
                 'vpage' : 'http://club.yule.sohu.com/fun_pics/thread/%s/?author=all&trash=0&page=%s',
                 'urls' : [
                     'http://club.yule.sohu.com/fun_pics/threads/#p1',
                     ] ,
     },
      {
                 'sid':6,
                 'f':'enjoy_suhu_com',
                 'urlpage' : r'/enjoy/thread/(.*?)/',
                 'page' : 'http://club.business.sohu.com/enjoy/thread/%s/',
                 'vpage' : 'http://club.business.sohu.com/enjoy/thread/%s/?author=all&trash=0&page=%s',
                 'urls' : [
                     'http://club.business.sohu.com/enjoy/threads/#p1',
                     ] ,
     },
     {
             'sid':7,
            'f':'jzhang01_suhu_com',
              'urlpage' : r'/jzhang01/thread/(.*?)/',
             'page' : 'http://club.money.sohu.com/jzhang01/thread/%s/',
              'vpage' : 'http://club.money.sohu.com/jzhang01/thread/%s/?author=all&trash=0&page=%s',
              'urls' : [
                   'http://club.money.sohu.com/jzhang01/threads/#p1',
               ] ,
    }
]
for __i in ding_rule:
    __i.update(__ding_rule)