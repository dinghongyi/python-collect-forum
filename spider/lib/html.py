#coding=utf-8
def pagelist(page,pagecount,c=10,pc=5,nc=4):
    ts = (pagecount+1-c if page+pc >pagecount else (1 if page < pc else page-nc))
    return range(ts if ts > 1 else 1,\
      pagecount+1 if page+pc >= pagecount else (c if (pagecount+1)>=c else (pagecount+1) ) +(page -(nc if page >pc else page-1)) + (-1 if page<nc and pagecount<10 else 0))