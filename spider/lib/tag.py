#coding=utf-8
import re,time
import lxml.html as HTML
def deltag(content):
    content = re.compile(r'<span name="\*\*(.*?)</span>',re.DOTALL).sub('',content)
    content = re.compile(r'<span name="3g_net_tag">(.*?)</span>',re.DOTALL).sub('',content)
    content = content.replace('src="http://txt.mop.com/ttv2/images/290.jpg"','') \
    .replace('src="http://static.tianyaui.com/img/static/2011/imgloading.gif"','') \
    .replace('data-original=','src=') \
    .replace('original=','src=') \
    .replace('name="subjContentImg"','') \
    .replace('</a>','') \
	.replace('height="120"','') \
    .strip()
    return filter_tags(content)

def filter_tags(htmlstr):
    htmlstr = (u'%s'%htmlstr).replace('<div>','').replace('</div>','').replace('<li>','')
    re_comment = re.compile(r'</?(div|iframe|DIV|IFRAME)?[^img|p|br|embed|i|b|IMG|P|BR|EMBED|I|B]+\w+[^>]+>')
    re_comment = re_comment.sub('',htmlstr)
    return re_comment

def formattime(timedate,s="%Y-%m-%d %H:%M:%S"):
    return time.strftime(s,time.gmtime(int(timedate) + 8*60*60))

def srcreplace(content):
    return content.replace('src="http://','src="http://127.0.0.1:8080/pic?path=http://').replace("src='http://","src='http://127.0.0.1:8080/pic?path=http://")

def getimg(data):
    re_comment = re.compile(r'<img.+>',re.IGNORECASE)
    cc = re.search(re_comment,data)
    if cc is not None:
        root = HTML.document_fromstring(data)
        a= root.xpath('//img')
        return a[0].get('src')
    return ''

